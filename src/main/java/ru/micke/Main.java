package ru.micke;
import java.util.Scanner;
import ru.micke.Converter.*;

public class Main {
    private static TemperatureConverter converter;
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        configure();
        double T = converter.convert(inputCelsium());
        System.out.printf("> Результат: %f", T);
    }

    protected static void configure(){
        System.out.println("> Выберете шкалу для преобразования температуры");
        System.out.println("> Кельвин: 1; Фаренгейт: 2; Ранкин:  3;");
        System.out.println("> Рёмер:   4; Ньютон:    5; Делиль:  6; Реомюр: 7");
        String str = scanner.next();
        if (str.equals("1")){
            converter = new KelvinConverter();
        } else if (str.equals("2")) {
            converter = new FahrenheitConverter();
        } else if (str.equals("3")) {
            converter = new RankineConverter();
        } else if (str.equals("4")) {
            converter = new ReomerConverter();
        } else if (str.equals("5")) {
            converter = new NewtonConverter();
        } else if (str.equals("6")) {
            converter = new DelisleConverter();
        } else if (str.equals("7")) {
            converter = new ReaumurConverter();
        } else {
            System.out.println("> неверное значение, повторите ввод");
            configure();
        }
    }
    protected static double inputCelsium() {
        System.out.println("> Введите температуры в градусах Цельсия");
        double ret;
        try {
            ret = scanner.nextDouble();
        } catch (java.util.InputMismatchException exp) {
            System.out.println("> Ошибка ввода, введите коректное значение");
            return inputCelsium();
        }
        return ret;
    }
}
