package ru.micke.Scales;

import ru.micke.Scales.Fahrenheit;

public class Rankine extends Fahrenheit {
    static {
        scaleConstantToCelsium = 491.67;
    }
}
