package ru.micke.Scales;

public class KelvinScale extends TempScale {
    static {
        scaleConstantToCelsium = 273.;
        scaleRatioToCelsium    = 1.;
    }
}
