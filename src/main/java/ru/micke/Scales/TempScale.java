package ru.micke.Scales;

public class TempScale {
    protected static double scaleConstantToCelsium = 0.;
    protected static double scaleRatioToCelsium    = 1.;

    public double convert(double celsiumT) {
            return celsiumT * scaleRatioToCelsium + scaleConstantToCelsium;
    }

    public double celsiumT(double gradT) {
        return  (gradT - scaleConstantToCelsium) / scaleRatioToCelsium;
    }
}
