package ru.micke.Scales;

public class Fahrenheit extends TempScale {
    static {
        scaleConstantToCelsium = 32.;
        scaleRatioToCelsium    = 1.8;
    }
}
