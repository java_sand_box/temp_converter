package ru.micke.Scales;

public class Delisle extends TempScale {
    static {
        scaleConstantToCelsium = 150.;
        scaleRatioToCelsium    = -1.5;
    }
}
