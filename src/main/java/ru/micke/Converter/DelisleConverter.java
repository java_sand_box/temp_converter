package ru.micke.Converter;

import ru.micke.Scales.TempScale;
import ru.micke.Scales.Delisle;

public class DelisleConverter extends TemperatureConverter {
    @Override
    public TempScale createScale() {
        return new Delisle();
    }
}
