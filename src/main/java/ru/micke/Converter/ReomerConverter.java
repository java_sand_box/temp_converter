package ru.micke.Converter;

import ru.micke.Scales.TempScale;
import ru.micke.Scales.Reomer;

public class ReomerConverter extends TemperatureConverter {
    @Override
    public TempScale createScale() {
        return new Reomer();
    }
}
