package ru.micke.Converter;

import ru.micke.Scales.TempScale;
import ru.micke.Scales.KelvinScale;

public class KelvinConverter extends TemperatureConverter {
    @Override
    public TempScale createScale() {
        return new KelvinScale();
    }
}
