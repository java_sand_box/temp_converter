package ru.micke.Converter;

import ru.micke.Scales.TempScale;
import ru.micke.Scales.Rankine;

public class RankineConverter extends TemperatureConverter {
    @Override
    public TempScale createScale() {
        return new Rankine();
    }
}
