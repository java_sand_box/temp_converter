package ru.micke.Converter;

import ru.micke.Scales.TempScale;
import ru.micke.Scales.Fahrenheit;

public class FahrenheitConverter extends TemperatureConverter{
    @Override
    public TempScale createScale() {
        return new Fahrenheit();
    }
}
