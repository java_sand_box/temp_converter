package ru.micke.Converter;

import ru.micke.Scales.TempScale;

public abstract class TemperatureConverter {
    public double convert(double celsiumT){
        TempScale scale = createScale();
        return scale.convert(celsiumT);
    }
    public abstract TempScale createScale();
}
