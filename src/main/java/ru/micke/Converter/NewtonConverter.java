package ru.micke.Converter;

import ru.micke.Scales.TempScale;
import ru.micke.Scales.Newton;

public class NewtonConverter extends TemperatureConverter {
    @Override
    public TempScale createScale() {
        return new Newton();
    }
}
