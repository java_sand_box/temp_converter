package ru.micke.Converter;

import ru.micke.Scales.TempScale;
import ru.micke.Scales.Reaumur;

public class ReaumurConverter extends TemperatureConverter {

    @Override
    public TempScale createScale() {
        return new Reaumur();
    }
}
